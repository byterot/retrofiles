@echo off

rem clear cmake cache
set "DIR=cmake"
if exist %DIR% (
	for /D %%p in ("%DIR%\*.*") do rmdir "%%p" /S /Q
	del %DIR%\*.* /F /Q
)

cd cmake
rem force cmake to use MinGW instead of visual studio (if installed)
cmake -G "MinGW Makefiles" -DMY_SYSTEM=WINDOWS -DCMAKE_TOOLCHAIN_FILE=toolchains/win32.cmake ..
mingw32-make

cd ..
copy cmake\retrofiles.exe bin\retrofiles.exe
