// #--------------------------------------------------------------------------#
// # Tiny webserver implementation                                            #
// # To serve a webpage to download files on a local network                  #
// # handy for older computers and mobile phones with no mass storage support #
// #                                                                          #
// # By bitrot                                                                #
// #--------------------------------------------------------------------------#

#include <iostream>
#include <fstream>
#include <string>
#include <boost/filesystem.hpp>
#include "webserver/server_http.hpp"
#include "utils.h"
#include <ctime>

using namespace std;
using HttpServer = SimpleWeb::Server<SimpleWeb::HTTP>;
using namespace boost::filesystem;

const char *serverpath = "/mnt/nas/Retro pcs/";
//const char *serverpath = "/mnt/data/nas/Retro pcs/";

string filename(string f)
{
  string fn = f;
  fn = fn.substr(fn.find_last_of("/\\") + 1);
  return fn;
}

void writeFileItem(string filepath, stringstream& stream)
{
  stream << "<div class=\"parent\">";
  stream << "<div class=\"text\">";
  stream << "<form action=\"download\" method=\"post\">";
  stream << "<input type=\"hidden\" name=\"file\"";
  stream << "value=\"" << filepath << "\">";
  stream << "<button type=\"submit\" class=\"btn-link\">";
  stream << filename(filepath);
  stream << "</button>";
  stream << "</form></div></div>";
}

void writeFolderItem(string path, stringstream& stream)
{
  stream << "<div class=\"parent\">";
  stream << "<div class=\"text\">";
  stream << "<form action=\"cd\" method=\"post\">";
  stream << "<input type=\"hidden\" name=\"path\"";
  stream << "value=\"" << path << "\">";
  stream << "<button type=\"submit\" class=\"btn-link\">";
  stream << filename(path);
  stream << "</button>";
  stream << "</form></div></div>";
}

void writeDirectory(string directory, stringstream& stream)
{
  path p (directory);

  directory_iterator end_itr;

  for(directory_iterator itr(p); itr != end_itr; ++itr)
  {  
    if(is_regular_file(itr->path()))
      writeFileItem(itr->path().string(), stream);
    else
      writeFolderItem(itr->path().string(), stream);
  }
}
void writeDirectoryPage(string directory, stringstream& stream)
{ 
    stream << "<html><head><title>Raspberry retro files</title><style>";
    stream << ".text-container{height:80px;width:100%;text-align:center;}";
    stream << ".text-container span{display:block;position:relative;top:50%;transform:translateY(-50%);color:#eee;}";
    stream << "</style></head><body>";

    stream << "<div class=\"text-container\" style=\"background-color: #000;\">";
    stream << "<span><a href=\"/\"><h1>Raspberry Retro Files</h1></a></span>";
    stream << "</div>";

    writeDirectory(directory, stream);

    stream << "</body></html>";
}
long filesize(string file)
{
  std::ifstream f(file, std::ifstream::in | std::ifstream::binary);
  if(!f) return -1;

  f.seekg(0, ios::end);
  long size = f.tellg();
  f.close();

  return size;
}

void writeFile(string filepath, shared_ptr<HttpServer::Response> response)
{
  string fn = filename(filepath);

  *response << "HTTP/1.1 200 OK\r\n";
  *response << "Pragma: public\r\n";
  *response << "Expires: 0\r\n";
  *response << "Cache-Control: public\r\n";
  *response << "Content-type: application/octet-stream\r\n";
  *response << "Content-Disposition: attachment; filename=\"" << fn << "\"\r\n";
  *response << "Content-Transfer-Encoding: binary\r\n";
/*  *response << "Content-Length: " << filesize(filepath) << "\r\n";
  *response << "\r\n";
  std::ifstream fs;
  fs.open(filepath);
  if(!fs)
  {
    cout << "Could not read: " << filepath << "\n";
    return;
  }
  *response << fs.rdbuf();
  fs.close();
*/
  long size = filesize(filepath);
  long sent = 0;
  *response << "Transfer-Encoding: chunked\r\n";
  *response << "\r\n";
  response->flush();
  std::ifstream fin(filepath, std::ifstream::binary);
  std:vector<char> buffer(1024 * 1024 * 8,0);
  while(!fin.eof())
  {
    fin.read(buffer.data(), buffer.size());
    std::streamsize s = fin.gcount();
    std::string d = std::string(buffer.data(), s);
    std::stringstream hex;
    hex << std::hex << s;
    *response << hex.str() << "\r\n" << d << "\r\n";
    
    sent += s;
    time_t now = time(0);   
    if(size > 100)
    cout << ctime(&now) << " -> sending " << fn << ": " << (sent / (size/100)) << "%\r\n";
    response->send();
  }
  *response << "0\r\n";
  cout << "Transfer of: " << fn << " finished\r\n";
  fin.close();
}


int main(int argc, char *argv[])
{
  HttpServer server;
  server.config.port = 8080;

  server.resource["^/download$"]["POST"] = [](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
    PostValues pv = parsePost(request->content.string());

    if(pv.size == 1 && pv.names.at(0) == "file" && 
      pv.values.at(0).find(serverpath) == 0)
    {
     writeFile(pv.values.at(0), response);
    }
    else
    {
      stringstream stream;
      stream << "<p>File not found<p>";
      response->write(stream);
    }
  };

  server.resource["^/cd$"]["POST"] = [](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
  auto content = request->content.string();
  PostValues pv = parsePost(content);

   stringstream stream;

    if(pv.size == 1 && pv.names.at(0) == "path" && 
      pv.values.at(0).find(serverpath) == 0)
    {
     writeDirectoryPage(pv.values.at(0), stream);
    }
    else
    {
      stringstream stream;
      stream << "<p>Path not found<p>";
      response->write(stream);
    }

   response->write(stream);
  };
  server.default_resource["GET"] = [](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
    stringstream stream;
    writeDirectoryPage(serverpath, stream);
    response->write(stream);
  };

  thread server_thread([&server]() {
      server.start();
      });    

  std::cout << "Server starting..." << std::endl;
  server_thread.join();
  return 0;
}
