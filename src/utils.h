#pragma once
#include <string>

std::string urlDecode(std::string &eString) {
	std::string ret;
	char ch;
	int i, j;
	for (i=0; i<eString.length(); i++) {
		if (int(eString[i])==37) {
			sscanf(eString.substr(i+1,2).c_str(), "%x", &j);
			ch=static_cast<char>(j);
			ret+=ch;
			i=i+2;
		} else {
			ret+=eString[i];
		}
	}
	return (ret);
}

void replaceAll(std::string& str, const std::string& from, const std::string& to) {
    if(from.empty())
        return;
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
    }
}

typedef struct {
  int size;
  std::vector<std::string> names;
  std::vector<std::string> values;
} PostValues;

PostValues parsePost(std::string post)
{
  PostValues r;

  std::stringstream ss(post);
  std::string pname, pvalue;
  while(std::getline(ss, pname, '='))
  {
    r.names.push_back(pname);
    std::getline(ss,pvalue);
    pvalue = urlDecode(pvalue);
    replaceAll(pvalue, "+", " ");
    r.values.push_back(pvalue);
  }
  
  r.size = r.values.size();  
  return r;
}
